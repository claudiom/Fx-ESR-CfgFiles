:: Firefox.exe Batch File provided by MDCPS ISS
:: Modified by Claudio Miranda for use with Firefox ESR.
:: 
:: Instructions: Modify the variables as shown below:
::    InstallerPath="<Your-Path-To-The-Firefox-Installer>" -- This can be a UNC path as well.
::    ConfigDirsPath="<Your-Path-To-The-Firefox-Config-Files-Folder>" -- This can be a UNC path as well.
::    FxVersion="<verson-number> ESR" -- Change the version number only, leave the ESR.

SET FxInstallerPath="<Your-Path-To-The-Firefox-Installer>"
SET ConfigDirsPath="<Your-Path-To-The-Firefox-Config-Files-Folder>"
SET FxVersion="<verson-number> ESR"

ver | find "7601" > nul
IF %ERRORLEVEL% == 0 GOTO Win7
ver | find "2600" > nul
IF %ERRORLEVEL% == 0 GOTO XP
GOTO END

:Win7
IF EXIST "C:\Program Files (x86)" GOTO WIN764
GOTO WIN732
GOTO END

:XP
%systemroot%\system32\reg.exe query "HKLM\SOFTWARE\Mozilla\Mozilla Firefox" /s |  find %FxVersion% > NUL
IF %ERRORLEVEL% == 1 GOTO INSTALLWINXP
GOTO END

:WIN732
%systemroot%\system32\reg.exe query "HKLM\SOFTWARE\Mozilla\Mozilla Firefox" /s |  find %FxVersion% > NUL
IF %ERRORLEVEL% == 1 GOTO INSTALLWIN7
GOTO END

:WIN764
%systemroot%\system32\reg.exe query "HKLM\SOFTWARE\Wow6432Node\Mozilla\Mozilla Firefox" /s |  find %FxVersion% > NUL
IF %ERRORLEVEL% == 1 GOTO INSTALLWIN7
GOTO END

:INSTALLWINXP
%systemroot%\system32\xcopy.exe %FxInstallerPath% "%systemroot%\temp" /Y
"%systemroot%\temp\Firefox Setup 38.2.0esr.exe" -ms
GOTO CONFIG32
GOTO END

:INSTALLWIN7
%FxInstallerPath% -ms
IF EXIST "C:\Program Files (x86)\Mozilla Firefox" GOTO CONFIG64
IF EXIST "C:\Program Files\Mozilla Firefox" GOTO CONFIG32
GOTO END


:CONFIG32
%systemroot%\system32\xcopy.exe "%ConfigDirsPath%\mozilla.cfg" "C:\Program Files\Mozilla Firefox\" /Y
%systemroot%\system32\xcopy.exe "%ConfigDirsPath%\channel-prefs.js" "C:\Program Files\Mozilla Firefox\defaults\pref\" /Y
GOTO END

:CONFIG64
%systemroot%\system32\xcopy.exe "%ConfigDirsPath%\mozilla.cfg" "C:\Program Files (x86)\Mozilla Firefox\" /Y
%systemroot%\system32\xcopy.exe "%ConfigDirsPath%\channel-prefs.js" "C:\Program Files (x86)\Mozilla Firefox\defaults\pref\" /Y
GOTO END


:END