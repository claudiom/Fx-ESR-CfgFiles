Firefox ESR Config Files. - Claudio Miranda
===========================================


Deployment batch file and configuration files for Firefox ESR on Windows.

Please make sure to modify the files as necessary before deploying.